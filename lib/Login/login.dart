import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../signup/signupPage.dart';

import '../home.dart';
//import 'signin_auth.dart';
import '../signup/googleSignIn.dart';
import '../signup/signupRestApi.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(30.0, 95.0, 0.0, 0.0),
                    child: Text('Hello',
                        style: TextStyle(
                            fontSize: 90.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(30.0, 170.0, 0.0, 0.0),
                    child: Text('你好',
                        style: TextStyle(
                            fontSize: 80.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(30.0, 260.0, 0.0, 0.0),
                    child: Text('안녕',
                        style: TextStyle(
                            fontSize: 70.0, fontWeight: FontWeight.bold)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(160.0, 265.0, 0.0, 0.0),
                    child: Text('.',
                        style: TextStyle(
                            fontSize: 90.0,
                            fontWeight: FontWeight.bold,
                            color: CupertinoColors.activeGreen)),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 100.0),
                  Container(
                      height: 50.0,
                      width: 300.0,
                      child: CupertinoButton(
                        borderRadius: BorderRadius.circular(30.0),
                        onPressed: () {
                          // _login();

                          signInWithGoogle().then((_) {
                            if (_ == 'SUCCESS') {
                              GoogleSignIn googleSignIn = GoogleSignIn();
                              googleSignIn.signInSilently().then((account) {
                                SignupRestApi().accountExist(account.id).then((isExist) {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(builder: (context) {
                                    return (isExist)
                                        ? Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                          return HomePage();
                                        }))
                                        : AlertDialog(
                                            content: Text(
                                                'Incorrect username or password.'),
                                          );
                                  }));
                                });
                              });
                            } else if (_ == "NO_CONNECTION")
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                return AlertDialog(
                                  content: Text(
                                      'Please make sure you have connection to the Internet'),
                                );
                              }));
                            else {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                return AlertDialog(
                                  content: Text('No account is selected'),
                                );
                              }));
                            }
                            // this.setState(() {
                            //   _isLoading = false;
                            // });
                          });

                          // signInWithGoogle(context).whenComplete(() {
                          //   Navigator.of(context)
                          //       .push(MaterialPageRoute(builder: (context) {
                          //     return HomePage();
                          //   }));
                          // });
                        },
                        pressedOpacity: 0.5,
                        color: CupertinoColors.activeGreen,
                        child: Text(
                          'LOGIN',
                          style: TextStyle(
                              fontFamily: 'Monterrat',
                              fontWeight: FontWeight.bold,
                              color: CupertinoColors.white),
                        ),
                      )),
                  // SizedBox(height: 90.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'New here?',
                        style: TextStyle(
                          fontFamily: 'Monsterrat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        width: 7.0,
                      ),
                      InkWell(
                        onTap: () {
                          print("tap");
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return SignupPage();
                          }));
                        },
                        child: Text(
                          'Register now',
                          style: TextStyle(
                              fontFamily: 'Monterrat',
                              fontWeight: FontWeight.bold,
                              color: CupertinoColors.activeGreen,
                              decoration: TextDecoration.underline),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ));
  }
}

import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import '../models/newsFeed.dart';
import './NewsFeedDetailPage.dart';

class AnnouncementPage extends StatefulWidget {
  AnnouncementPage({Key key}) : super(key: key);

  @override
  AnnouncementPageState createState() => AnnouncementPageState();
}

class AnnouncementPageState extends State<AnnouncementPage> {
  static final baseURL = "http://localhost:3001";
  static final newsFeedURL = baseURL + "/newsFeedCreate";
  static final getNewsFeed = newsFeedURL + "/getAnnoucementList";

  List data;

  Future<dynamic> getData() async {
    var response = await http.get(_localhost());

    try {
      this.setState(() {
        var extractData = json.decode(response.body);
        data = extractData["data"];
      });
      print(data);
      return "Success!";
    } catch (error) {
      print("ErrorGetData: $error");
    }
  }

  String _localhost() {
    if (Platform.isAndroid)
      return getNewsFeed;
    else // for iOS simulator
      return getNewsFeed;
  }

  @override
  void initState() {
    this.getData();
    print("hi,annoucement");
  }

  _passDataToPage(int id, String title, String description, String author,
      String category, Uint8List bytes2, String createDT) async {
    NewsFeed newsFeed = new NewsFeed(
        id, title, description, category, author, bytes2, createDT);
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NewsFeedDetailPage(
                  newsFeed: newsFeed,
                )));
  }

  Widget _displayCard(int index) {
    var id = data[index]["ID"];
    var title = data[index]["title"];
    var description = data[index]["description"];
    var author = data[index]["author"];
    var category = data[index]["category"];
    Uint8List bytes2 = base64.decode(data[index]["img_btoa_in_text"]);
    var createDT = data[index]["created_DT"];

    return InkWell(
      splashColor: Colors.blue,
      onTap: () {
        _passDataToPage(
            id, title, description, author, category, bytes2, createDT);
      },
      child: Card(
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(0.0)),
        ),
        margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 7.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                Image.memory(
                  bytes2,
                  height: 130.0,
                  width: 130.0,
                  fit: BoxFit.fill,
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: 10.0,
                  ),
                ),
              ],
            ),
            Container(
              child: Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 26.0,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Announcement'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Expanded(
              child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return _displayCard(index);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:math';
import 'package:flutter/material.dart';
import "../models/newsFeed.dart";

class NewsFeedDetailPage extends StatefulWidget {
  NewsFeedDetailPage({Key key, this.newsFeed}) : super(key: key);
  final NewsFeed newsFeed;

  @override
  NewsFeedDetailPageState createState() => NewsFeedDetailPageState();
}

class NewsFeedDetailPageState extends State<NewsFeedDetailPage> {
  @override
  Widget build(BuildContext context) {
    var createdDateTime = this.widget.newsFeed.createdDate;
    DateTime localTime =
        DateTime.parse("${createdDateTime.substring(0, 19)}-0800");
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          this.widget.newsFeed.title.toString(),
          style: TextStyle(
            fontWeight: FontWeight.w700,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                child: Column(
              children: <Widget>[
                Text(
                  this.widget.newsFeed.title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 35.0,
                  ),
                  maxLines: 6,
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                  child: Divider(
                    color: Colors.black,
                    height: 36,
                  ),
                ),
              ],
            )),
            Container(
              margin: EdgeInsets.only(
                bottom: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.access_time,
                    color: Colors.grey,
                    size: 30.0,
                  ),
                  Text(
                    localTime.toUtc().toString().substring(0, 19),
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                    ),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              child: Image.memory(
                this.widget.newsFeed.imgBtoa,
                height: 250.0,
                width: 350.0,
                fit: BoxFit.fill,
              ),
            ),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Text(
                this.widget.newsFeed.description,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.justify,
                // maxLines: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:typed_data';

import 'package:flutter/material.dart';
import './AnnouncementPage.dart';
import './KnowledgePage.dart';
import './ActivityPage.dart';
import '../models/newsFeed.dart';
import './NewsFeedDetailPage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class NewsFeedPage extends StatefulWidget {
  NewsFeedPage({Key key}) : super(key: key);

  @override
  NewsFeedPageState createState() => NewsFeedPageState();
}

class NewsFeedPageState extends State<NewsFeedPage> {
  static final baseURL = "http://localhost:3001";
  static final newsFeedURL = baseURL + "/newsFeedCreate";
  static final getNewsFeed = newsFeedURL + "/getNewsFeed";

  List data;

  Future<dynamic> getData() async {
    var response = await http.get(_localhost());

    try {
      this.setState(() {
        var extractData = json.decode(response.body);
        data = extractData["data"];
      });
      print(data);
      return "Success!";
    } catch (error) {
      print("ErrorGetData: $error");
    }
  }

  String _localhost() {
    if (Platform.isAndroid)
      return getNewsFeed;
    else // for iOS simulator
      return getNewsFeed;
  }

  @override
  void initState() {
    this.getData();
    print("hi");
  }

  _passDataToPage(int id, String title, String description, String author,
      String category, Uint8List bytes2, String createDT) async {
    NewsFeed newsFeed = new NewsFeed(
        id, title, description, category, author, bytes2, createDT);
    print("hi");
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NewsFeedDetailPage(
                  newsFeed: newsFeed,
                )));
  }

  Widget _displayCard(int index) {
    var id = data[index]["ID"];
    var title = data[index]["title"];
    var description = data[index]["description"];
    var author = data[index]["author"];
    var category = data[index]["category"];
    Uint8List bytes2 = base64.decode(data[index]["img_btoa_in_text"]);
    var createDT = data[index]["created_DT"];

    return InkWell(
      splashColor: Colors.blue,
      onTap: () {
        _passDataToPage(
            id, title, description, author, category, bytes2, createDT);
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 7.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                Image.memory(
                  bytes2,
                  height: 130.0,
                  width: 130.0,
                  fit: BoxFit.fill,
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: 10.0,
                  ),
                ),
              ],
            ),
            Container(
              child: Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 26.0,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Language Learning Tool"),
        elevation: 0.0,
        // actions: <Widget>[
        //   IconButton(
        //     icon: Icon(Icons.history),
        //     // onPressed: () {
        //     //   Navigator.push(
        //     //     context,
        //     //     MaterialPageRoute(builder: (context) => Hisotry()),
        //     //   );
        //     // },
        //   ),
        // ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              top: 15.0,
              bottom: 15.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    RawMaterialButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AnnouncementPage()),
                        );
                      },
                      child: Icon(
                        Icons.announcement,
                        color: Colors.blue,
                        size: 55.0,
                      ),
                      shape: CircleBorder(),
                      elevation: 2.0,
                      fillColor: Colors.white,
                      padding: const EdgeInsets.all(15.0),
                    ),
                    Text(
                      "Announcement",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RawMaterialButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => KnowledgePage()),
                        );
                      },
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.blue,
                        size: 55.0,
                      ),
                      shape: CircleBorder(),
                      elevation: 2.0,
                      fillColor: Colors.white,
                      padding: const EdgeInsets.all(15.0),
                    ),
                    Text(
                      "Knowledge",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    RawMaterialButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActivityPage()),
                        );
                      },
                      child: Icon(
                        Icons.event,
                        color: Colors.blue,
                        size: 55.0,
                      ),
                      shape: CircleBorder(),
                      elevation: 2.0,
                      fillColor: Colors.white,
                      padding: const EdgeInsets.all(15.0),
                    ),
                    Text(
                      "Activity",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            child: Expanded(
              child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return _displayCard(index);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

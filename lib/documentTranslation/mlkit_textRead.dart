import 'dart:convert';
import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
//import './assets/sampleJSON.json' as sampleJSON;
import './parsingJSON.dart';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';

class MLKitTextRead extends StatefulWidget {
  @override
  _MLKitTextReadState createState() => _MLKitTextReadState();
}

class _MLKitTextReadState extends State<MLKitTextRead> {
  File _pickedImage;
  bool _isImageLoaded = false;
  static String _recognizedText = "";
  String _translatedText = "";
  String _detectedLanguage = "";
  String _definition;
  bool _error = false;

  static String api_KEY = "AIzaSyBJlyRaGXkPP5fW1qXHplSjHEk2ciX9uak";
  static String language = "en";

  Future pickImage() async {
    var tempStore = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _pickedImage = tempStore;
      _isImageLoaded = true;
    });
  }

  Future readText() async {
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_pickedImage);
    TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
    VisionText readText = await recognizeText.processImage(ourImage);

    String tempText = "";
    for (TextBlock block in readText.blocks) {
      for (TextLine line in block.lines) {
        for (TextElement word in line.elements) {
          tempText += word.text + " ";
        }
      }
    }
    setState(() {
      _recognizedText = tempText;
    });
    callTranslateAPI();
  }

  Future<String> loadJSONfile() async {
    return await rootBundle.loadString('assets/sampleJSON.json');
  }

  Future callTranslateAPI() async {
    String url =
        'https://translation.googleapis.com/language/translate/v2?target=${language}&key=${api_KEY}&q=${_recognizedText}';
    final response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    final jsonResponse = json.decode(response.body);
    Json dataJson = new Json.fromJson(jsonResponse);

    if (response.statusCode == 200) {
      print("Translated Text: ${dataJson.data.translations[0].translatedText}");
      print(
          "Detected language: ${dataJson.data.translations[0].detectedSourceLanguage}");
    } else {
      throw Exception('Failed to translate text');
    }
    setState(() {
      _translatedText = dataJson.data.translations[0].translatedText;
      _detectedLanguage = dataJson.data.translations[0].detectedSourceLanguage;
    });
  }

  _launchURL() async {
    const url = 'https://developer.oxforddictionaries.com/documentation/response-codes';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future callDictionaryAPI(String text) async {
    text = text.replaceAll(new RegExp(r'[^\w]+'),'');
    print("What I have clicked: " + text);
    final String language = "en";
    final String word_id = text;
    final String url = 'https://od-api.oxforddictionaries.com:443/api/v2/entries/${language}/${word_id.toLowerCase()}';

    final response = await http
      .get(Uri.encodeFull(url), 
        headers: {
          "app_id": "98a1de01",
          "app_key": "7e0a8b67594c322ef6828ad2a7099524"
        });
    print("Status code: " + response.statusCode.toString());
    if (response.statusCode == 200) {
      final jsonResponse = json.decode(response.body);

      List<Results> list;
      var rest = jsonResponse["results"] as List;
      list = rest.map<Results>((i) => Results.fromJson(i)).toList();
      //print("${list[0].lexicalEntries[0].entries[0].senses[0].shortDefinitions[0]}");
      setState(() {
        _error = false;
        _definition = list[0].lexicalEntries[0].entries[0].senses[0].shortDefinitions[0];
      });
      _showSnackBar(_definition);
    } else {
      setState(() {
        _error = true;
      });
      _showSnackBar("#" + response.statusCode.toString());
    }
  }

  _showSnackBar(String text) {
    final snackBar = SnackBar(
      duration: Duration(hours: 10),
      content: _error? Row(
        children: <Widget>[
          Icon(
            Icons.warning
          ),
          SizedBox(width: 5.0),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Oops! Error '
                ),
                TextSpan(
                  text: text,
                  style: TextStyle(
                    decoration: TextDecoration.underline
                  ),
                  recognizer: TapGestureRecognizer() 
                    ..onTap = () {
                      _launchURL();
                    }
                ),
                TextSpan(
                  text: ' 🙁'
                )
              ]
            ),
          )
        ],
      ): 
      Row(
        children: <Widget>[
          Icon(
            Icons.info_outline
          ),
          SizedBox(width: 5.0),
          Text(text)
        ],
      ),
            
      
          
        
      action: SnackBarAction(
        label: 'Got it!',
        onPressed: () {},
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 80.0),
              _isImageLoaded
                  ? Center(
                      child: Container(
                        height: 200.0,
                        width: 350.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black38,
                            width: 3.0,
                            style: BorderStyle.solid
                          ),
                          borderRadius: BorderRadius.all( Radius.circular(6.0)),
                          image: DecorationImage(
                              image: FileImage(_pickedImage), fit: BoxFit.contain)),
                      ),
                    )
                  : Container(),
              SizedBox(height: 10.0),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        onPressed: pickImage,
                        child: Text('Pick an image'),
                      ),
                      SizedBox(width: 30.0),
                      RaisedButton(onPressed: readText, child: Text('Read Text')),
                    ],
                  ),
                  DropdownButton<String>(
                      items: [
                        DropdownMenuItem<String>(
                          child: Text("English"),
                          value: 'en',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Arabic"),
                          value: 'ar',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Chinese"),
                          value: 'zh',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("French"),
                          value: 'fr',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("German"),
                          value: 'de',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Indonesian"),
                          value: 'id',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Italian"),
                          value: 'it',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Japanese"),
                          value: 'ja',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Korean"),
                          value: 'ko',
                        ),
                        DropdownMenuItem<String>(
                          child: Text("Spanish"),
                          value: 'es',
                        ),
                      ],
                      onChanged: (String target) {
                        setState(() {
                          language = target;
                        });
                      },
                      hint: Text('Target Language'),
                      value: language)
                ],
              ),
              SizedBox(height: 10.0),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Detected language: " + _detectedLanguage.toUpperCase(),
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0),
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      "Text: ",
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0),
                    ),
                    Wrap(
                      children: _translatedText.split(' ').map((String text) => 
                        InkWell(
                          child: Text.rich(
                            TextSpan(
                              text: text,
                              style: TextStyle(
                                fontWeight: FontWeight.w400, 
                                fontSize: 20.0, 
                                decoration: TextDecoration.underline, 
                                decorationColor: CupertinoColors.activeGreen, 
                                decorationThickness: 1.5),
                              children: <TextSpan> [
                                TextSpan(
                                  text: " ",
                                  style: TextStyle(
                                    fontSize: 20.0, 
                                    decoration: TextDecoration.none, 
                                  ),
                                )
                              ]
                            ) 
                          ),
                          onTap: () {
                            callDictionaryAPI(text);
                          },
                        ))
                        .toList()
                    ),
                  ],
                ),
                padding: EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                alignment: Alignment.center,
              )
            ],
          ),
      )
    );
  }
}
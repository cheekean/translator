class Json {
  Data data;
  Json({this.data});
  factory Json.fromJson(Map<String, dynamic> json) {
    return Json(data: Data.fromJson(json['data']));
  }
}

class Data {
  final List<Translations> translations;
  Data({this.translations});

  factory Data.fromJson(Map<String, dynamic> json) {
    var list = json['translations'] as List;
    print(list.runtimeType);
    List<Translations> translationList =
        list.map((i) => Translations.fromJson(i)).toList();
    return Data(translations: translationList);
  }
}

class Translations {
  String translatedText, detectedSourceLanguage;

  Translations({this.detectedSourceLanguage, this.translatedText});
  factory Translations.fromJson(Map<String, dynamic> json) {
    return Translations(
        translatedText: json['translatedText'],
        detectedSourceLanguage: json['detectedSourceLanguage']);
  }
}



class Results {
  final List<LexicalEntries> lexicalEntries;
  Results({this.lexicalEntries});

  factory Results.fromJson(Map<String, dynamic> json) {
    var list = json['lexicalEntries'] as List;
    List<LexicalEntries> lexicalEntriesList = list.map((i) => LexicalEntries.fromJson(i)).toList();
    return Results(lexicalEntries: lexicalEntriesList);
  }
}

class LexicalEntries {
  final List<Entries> entries;
  LexicalEntries({this.entries});

  factory LexicalEntries.fromJson(Map<String, dynamic> json) {
    var list = json['entries'] as List;
    List<Entries> entriesList = list.map((i) => Entries.fromJson(i)).toList();
    return LexicalEntries(entries: entriesList);
  }
}

class Entries {
  final List<Senses> senses;
  Entries({this.senses});

  factory Entries.fromJson(Map<String, dynamic> json) {
    var list = json['senses'] as List;
    List<Senses> sensesList = list.map((i) => Senses.fromJson(i)).toList();
    return Entries(senses: sensesList);
  }
}

class Senses {
  final List<String> shortDefinitions;
  Senses({this.shortDefinitions});

  factory Senses.fromJson(Map<String, dynamic> json) {
    List<String> shortDefinitionsList = json['shortDefinitions'].cast<String>();
    return Senses(shortDefinitions: shortDefinitionsList);
  }
}
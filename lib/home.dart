import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:titled_navigation_bar/titled_navigation_bar.dart';

import './home_pages/page1.dart';
import './home_pages/page2.dart';
import './home_pages/page3.dart';
import './home_pages/page4.dart';
import './home_pages/page5.dart';
import './home_pages/page6.dart';

// import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentTab = 0;
  Page1 one;
  Page2 two;
  Page3 three;
  Page4 four;
  Page5 five;
  Page6 six;
  List<Widget> pages;
  Widget currentPage;

  @override
  void initState() {
    one = Page1();
    two = Page2();
    three = Page3();
    four = Page4();
    five = Page5();
    six = Page6();

    pages = [one, two, three, four, five, six];
    currentPage = one;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Future.value(false),
        child: Scaffold(
          body: currentPage,
          bottomNavigationBar: TitledBottomNavigationBar(
            currentIndex: currentTab,
            onTap: (index) {
              setState(() {
                currentTab = index;
                currentPage = pages[index];
              });
            },
            reverse: true,
            indicatorColor: CupertinoColors.activeGreen,
            activeColor: CupertinoColors.activeGreen,
            items: [
              TitledNavigationBarItem(title: 'Home', icon: Icons.home),
              TitledNavigationBarItem(
                  title: 'Text Translate', icon: Icons.translate),
              TitledNavigationBarItem(
                  title: 'Voice Translation', icon: Icons.mic),
              TitledNavigationBarItem(title: 'Document', icon: Icons.image),
              TitledNavigationBarItem(title: 'Phrasebook', icon: Icons.book),
              // TitledNavigationBarItem(
              //     title: 'Profile', icon: Icons.person_outline),
            ],
          ),
        ));
  }
}

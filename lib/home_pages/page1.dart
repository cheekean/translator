import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../NewsFeed/newsFeedPage.dart';

class Page1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(child: NewsFeedPage());
  }
}

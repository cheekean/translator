import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../textTranslation/TextTranslate_Page.dart';

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(child: TextTranslatePage());
  }
}

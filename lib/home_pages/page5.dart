import 'package:LanguageTranslator/phrasebook/phrasebookMainPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import '../documentTranslation/mlkit_textRead.dart';

class Page5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(child: PhrasebookHome());
  }
}

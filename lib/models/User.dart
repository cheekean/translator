class User {
  final String uid,
      matricNum,
      contactNum,
      role,
      gender,
      name,
      dob,
      createdDt,
      modifiedDt;
  final int approved;
  bool selected = false;

  User(this.uid, this.approved, this.matricNum, this.contactNum, this.role,
      this.gender, this.name, this.dob, this.createdDt, this.modifiedDt);
  User.fromJson(Map<String, dynamic> data)
      : uid = data['uid'],
        approved = data['approved'],
        matricNum = data['matric_num'],
        contactNum = data['contact_num'],
        role = data['role'],
        gender = data['gender'],
        name = data['name'],
        dob = data['dob'],
        createdDt = data['createdDt'],
        modifiedDt = data['modifiedDt'];
}

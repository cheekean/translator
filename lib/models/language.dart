class Language {
  String name;
  String code;
  String voiceCode;
  bool isRecent;
  bool isDownloaded;
  bool isDownloadable;

  Language(String code, String name, bool isRecent, bool isDownloaded,
      bool isDownloadable, String voiceCode) {
    this.name = name;
    this.code = code;
    this.isRecent = isRecent;
    this.isDownloaded = isDownloaded;
    this.isDownloadable = isDownloadable;
    this.voiceCode = voiceCode;
  }
}

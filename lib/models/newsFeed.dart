import 'dart:typed_data';

class NewsFeed {
  int id;
  String title;
  String description;
  String category;
  String author;
  Uint8List imgBtoa;
  String createdDate;

  NewsFeed(int id, String title, String description, String category,
      String author, Uint8List imgBtoa, String createdDate) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.category = category;
    this.author = author;
    this.imgBtoa = imgBtoa;
    this.createdDate = createdDate;
  }

  void setTitle(String title) {
    this.title = title;
  }
}

class Translate {
  String text;
  String translatedText;
  bool isStarred;

  Translate(String text, String translatedText, bool isStarred) {
    this.text = text;
    this.translatedText = translatedText;
    this.isStarred = isStarred;
  }

  void setStarred(bool isStarred) {
    this.isStarred = isStarred;
  }

//    Translate({
//     this.text,
//     this.translatedText,
//     this.isStarred
//  });
}

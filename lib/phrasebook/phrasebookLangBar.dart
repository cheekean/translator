import 'package:LanguageTranslator/models/language.dart';
import 'package:LanguageTranslator/voiceTranslation/Languages.dart';
import 'package:flutter/material.dart';

class PhrasebookLangBar extends StatefulWidget {
  final Function(String) update;
  PhrasebookLangBar({@required this.update});

  @override
  State<StatefulWidget> createState() {
    return _PhrasebookLangBarState();
  }
}

class _PhrasebookLangBarState extends State<PhrasebookLangBar> {
  String outputLang = "zh";
  List languageList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Languages.comLan.forEach((key, value) {
      languageList.add({"code": key, "name": value['name']});
      print("langeages" + languageList.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Translate to: ",
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
        Container(
          child: DropdownButton<String>(
              value: outputLang,
              icon: Icon(Icons.keyboard_arrow_down),
              iconSize: 24,
              elevation: 16,
              // style: TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.grey,
              ),
              onChanged: (String newValue) {
                print(newValue);
                setState(() {
                  // print(widget.outputLang);

                  outputLang = newValue;
                });
                widget.update(outputLang);
              },
              items: languageList.map((f) {
                return DropdownMenuItem<String>(
                  value: f["code"],
                  child: Text(f["name"]),
                );
              }).toList()),
        )
      ],
    );
  }
}

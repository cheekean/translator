import 'package:LanguageTranslator/phrasebook/phrasebookLangBar.dart';
import 'package:LanguageTranslator/phrasebook/phrasebookRestApi.dart';
import 'package:LanguageTranslator/voiceTranslation/Languages.dart';
import 'package:LanguageTranslator/voiceTranslation/TTSHelper.dart';
import 'package:flutter/material.dart';
import 'package:translator/translator.dart';

GoogleTranslator translator = GoogleTranslator();

class PhrasebookHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PhrasebookPageState();
  }
}

class Phrase {
  String classification;
  String item;
  Phrase.toJson(Map<String, dynamic> data)
      : classification = data["classification"],
        item = data["item"];
}

class PhraseView {
  String classification;
  List items = [];
  PhraseView(this.classification);
}

class _PhrasebookPageState extends State<PhrasebookHome> {
  PhrasebookRestApi phrasebookRestApi = PhrasebookRestApi();
  List<Phrase> phrases = [];
  List<PhraseView> entries = [];
  List<String> classes = [];
  String outputLang = "zh";

  @override
  void initState() {
    super.initState();
    // print(phraseBookLangBar.outputLang + " loll");
    phrasebookRestApi.getAllPhrase().then((onValue) {
      print(onValue[0]);
      // print(JsonDecoder(onValue));
      for (int i = 0; i < onValue.length; i++) {
        Phrase phrase = Phrase.toJson(onValue[i]);
        if (!classes.any((e) => e.contains(phrase.classification))) {
          classes.add(phrase.classification);
          entries.add(PhraseView(phrase.classification));
        }
        entries
            .firstWhere((e) => e.classification == phrase.classification)
            .items
            .add(phrase.item);
      }
      print(entries);
      setState(() {
        entries = entries;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    PhrasebookRestApi phrasebookRestApi = PhrasebookRestApi();
    PhrasebookLangBar phraseBookLangBar = PhrasebookLangBar(update: update);
    phrasebookRestApi.getAllPhrase();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Phrasebook'),
          ),
          body: Column(
            children: <Widget>[
              // Text((outputLang + " loll")),
              Container(height: 50, child: phraseBookLangBar),
              Expanded(
                // height: MediaQuery.of(context).size.height * 0.70,
                child: ListView.builder(
                  itemBuilder: (BuildContext context, int index) =>
                      EntryItem(entries[index], outputLang),
                  itemCount: entries.length,
                ),
              )
            ],
          )),
    );
  }

  update(String lang) {
    setState(() {
      outputLang = lang;
    });
  }
}

// One entry in the multilevel list displayed by this app.
class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;

  Entry.fromJson(Map<String, dynamic> data)
      : title = data["classification"],
        children = data["item"];
}

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
  const EntryItem(this.entry, this.outputLang);
  final String outputLang;
  final PhraseView entry;

  Widget _buildTiles(PhraseView root) {
    List<Widget> list = new List<Widget>();
    print("out" + outputLang.toString());
    for (int i = 0; i < root.items.length; i++) {
      translator
          .translate(root.items[i].toString(),
              from: Languages.comLan["en"]["translate"],
              to: Languages.comLan[outputLang]["translate"])
          .then((output) => {
                list.add(new ListTile(
                  title: Text(root.items[i]), // + "\r\n" + output),
                  // leading: Icon(Icons.volume_up),
                  subtitle: Text(output),
                  trailing: IconButton(
                    icon: Icon(Icons.volume_up),
                    onPressed: () {
                      print("speaking");
                      TtsHelper.instance.setLanguageAndSpeak(
                          output, Languages.comLan[outputLang]["tts"]);
                    },
                  ),
                  // dense: true,
                ))
                // setState(() => resultText = output.toString()),
                // TtsHelper.instance.setLanguageAndSpeak(
                //     output, Languages.comLan[outputLang]["tts"]),
                // voiceTranslationRestApi.insertNewVoiceTranslation(
                //     "",
                //     textRecog,
                //     output,
                //     Languages.comLan[inputLang]["name"],
                //     Languages.comLan[outputLang]["name"],
                //     context)
              });
    }
    if (root.items.isEmpty) return ListTile(title: Text(root.classification));
    return ExpansionTile(
      key: PageStorageKey<PhraseView>(root),
      title: Text(root.classification),
      children: list,
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}

import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utils/networkUtil.dart';

class PhrasebookRestApi {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL =
      // "http://localhost:3001";
      // "http://192.168.43.167:3001";
      "http://localhost:3001";
  static final SELECT_URL = BASE_URL + "/phrasebook/getAllPhrase";

  // Future<void> insertNewVoiceTranslation(
  //     String uid,
  //     String textRecog,
  //     String textTranslated,
  //     String inputLang,
  //     String outputLang,
  //     BuildContext context) {
  //   String message = "";
  //   var body = json.encode({
  //     "uid": "DG7cQzfyR3OnFqMk0KmcufzfuIL",
  //     "textRecog": textRecog,
  //     "textTranslated": textTranslated,
  //     "inputLang": inputLang,
  //     "outputLang": outputLang
  //   });
  //   return _netUtil
  //       .post(INSERT_URL, body: body.toString())
  //       .then((int res) async {
  //     if (res == 10) {
  //       final snackBar = SnackBar(
  //           content: Row(children: <Widget>[
  //         Icon(Icons.settings_voice),
  //         Text('Insert into database failed, try again later'),
  //       ]));
  //       Scaffold.of(context).showSnackBar(snackBar);
  //     } else if (res == 0) {}
  //   });
  // }

  Future<dynamic> getAllPhrase() {
    String message = "";
    return _netUtil.get(SELECT_URL).then((dynamic res) {
      return res;
    });
  }
}

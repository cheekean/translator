import 'package:data_tables/data_tables.dart';
import 'package:flutter/material.dart';
import '../signup/signupRestApi.dart';
import '../models/User.dart';

class Dt extends StatefulWidget {
  @override
  _DtState createState() => _DtState();
}

class _DtState extends State<Dt> {
  final navigatorKey = GlobalKey<NavigatorState>();

  void show() {
    final context = navigatorKey.currentState.overlay.context;
    final dialog = AlertDialog(
      content: Text('Test'),
    );
    showDialog(context: context, builder: (x) => dialog);
  }

  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  int _sortColumnIndex;
  bool _sortAscending = true;
  bool isLoading = true;
  bool isEmpty = false;
  List<User> tutors = [];
  @override
  void initState() {
    super.initState();
    tutors = [];
    new SignupRestApi().getAllPendingTutor().then((onValue) {
      for (var i = 0; i < onValue.length; i++) {
        tutors.add(User.fromJson(onValue[i]));
      }
      setState(() {
        isLoading = false;
        if (tutors.length == 0)
          isEmpty = true;
        else
          isEmpty = false;
      });
      _items = tutors;
    });
    // _items = _Accounts;
  }

  void _sort<T>(
      Comparable<T> getField(User d), int columnIndex, bool ascending) {
    _items.sort((User a, User b) {
      if (!ascending) {
        final User c = a;
        a = b;
        b = c;
      }
      final Comparable<T> aValue = getField(a);
      final Comparable<T> bValue = getField(b);
      return Comparable.compare(aValue, bValue);
    });
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  List<User> _items = [];
  int _rowsOffset = 0;

  @override
  Widget build(BuildContext context) {
    print("length");
    print(_items.length);
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Pending Tutor List'),
        ),
        body: (_items.length == 0)
            ? Text('No data found')
            : NativeDataTable.builder(
                rowsPerPage: _rowsPerPage,
                itemCount: _items?.length ?? 0,
                firstRowIndex: _rowsOffset,
                handleNext: () async {
                  setState(() {
                    _rowsOffset += _rowsPerPage;
                  });

                  await new Future.delayed(new Duration(seconds: 3));
                },
                handlePrevious: () {
                  setState(() {
                    _rowsOffset -= _rowsPerPage;
                  });
                },
                itemBuilder: (int index) {
                  print(index);
                  final User account = _items[index];
                  return DataRow.byIndex(
                      index: index,
                      selected: account.selected,
                      onSelectChanged: (bool value) {
                        if (account.selected != value) {
                          setState(() {
                            account.selected = value;
                          });
                        }
                      },
                      cells: <DataCell>[
                        DataCell(Text('${account.name}')),
                        DataCell(Text('${account.matricNum}')),
                        DataCell(Text('${account.gender}')),
                        DataCell(Text('${account.dob}'.split('T')[0])),
                        DataCell(Text('${account.contactNum}')),
                        DataCell(Text(
                            '${account.createdDt.replaceFirst("T", " ").split(".")[0]}')),
                      ]);
                },
                header: const Text('Pending Tutors'),
                sortColumnIndex: _sortColumnIndex,
                sortAscending: _sortAscending,
                onRowsPerPageChanged: (int value) {
                  setState(() {
                    _rowsPerPage = value;
                  });
                },
                onSelectAll: (bool value) {
                  for (var row in _items) {
                    setState(() {
                      row.selected = value;
                    });
                  }
                },
                rowCountApproximate: true,
                selectedActions: <Widget>[
                  IconButton(
                    tooltip: "Verify",
                    icon: Icon(Icons.check),
                    onPressed: () {
                      List<String> uidList = [];
                      for (var item in _items
                          ?.where((d) => d?.selected ?? false)
                          ?.toSet()
                          ?.toList()) {
                        uidList.add(item.uid);
                      }
                      new SignupRestApi()
                          .verifyTutor(uidList, context)
                          .then((val) {
                        var message = (val == 0)
                            ? "The tutor(s) is/are verified"
                            : "Please try again later";
                        return showDialog(
                          context: context,
                          builder: (context) {
                            tutors = [];
                            new SignupRestApi()
                                .getAllPendingTutor()
                                .then((onValue) {
                              for (var i = 0; i < onValue.length; i++) {
                                tutors.add(User.fromJson(onValue[i]));
                              }
                              setState(() {
                                _items = tutors;
                              });
                              return null;
                            });
                            return AlertDialog(
                              content: Text(message),
                            );
                          },
                        );
                      });
                    },
                  ),
                  IconButton(
                    tooltip: "Reject",
                    icon: Icon(Icons.not_interested),
                    onPressed: () {
                      List<String> uidList = [];
                      for (var item in _items
                          ?.where((d) => d?.selected ?? false)
                          ?.toSet()
                          ?.toList()) {
                        uidList.add(item.uid);
                      }
                      new SignupRestApi().rejectTutor(uidList).then((val) {
                        var message = (val == 0)
                            ? "The tutor(s) is/are rejected"
                            : "Please try again later";
                        return showDialog(
                          context: context,
                          builder: (context) {
                            tutors = [];
                            new SignupRestApi()
                                .getAllPendingTutor()
                                .then((onValue) {
                              for (var i = 0; i < onValue.length; i++) {
                                tutors.add(User.fromJson(onValue[i]));
                              }
                              setState(() {
                                _items = tutors;
                              });
                              return null;
                            });
                            return AlertDialog(
                              content: Text(message),
                            );
                          },
                        );
                      });
                    },
                  ),
                ],
                columns: <DataColumn>[
                  DataColumn(
                      label: const Text('Name'),
                      onSort: (int columnIndex, bool ascending) =>
                          _sort<String>(
                              (User d) => d.uid, columnIndex, ascending)),
                  DataColumn(
                    label: const Text('Matric Number'),
                  ),
                  DataColumn(
                    label: const Text('Gender'),
                  ),
                  DataColumn(
                    label: const Text('Date of Birth'),
                  ),
                  DataColumn(
                    label: const Text('Contact Number'),
                  ),
                  DataColumn(
                    label: const Text('Created On'),
                  ),
                ],
              ),
      ),
    );
  }
}

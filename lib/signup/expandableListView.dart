import 'package:flutter/material.dart';
import 'signupRestApi.dart';

import '../models/User.dart';

class ExpandableListView extends StatefulWidget {
  @override
  _ExpandableListView createState() => _ExpandableListView();
}

class _ExpandableListView extends State<ExpandableListView> {
  static final List<String> _listViewData = [
    "1ndasa.com",
    "Flutter Dev",
    "Android Dev",
    "iOS Dev!",
    "React Native Dev!",
    "React Dev!",
    "express Dev!",
    "Laravel Dev!",
    "Angular Dev!",
  ];
  List<User> tutors = [];
  bool isLoading = true;
  List<Row> _builtRow = [];
  List<ExpansionTile> _listOfExpansions1 = [];
  List tutorsSelected = [];
  ListView tutorsSelectedList;
  @override
  void initState() {
    super.initState();
    tutors = [];
    new SignupRestApi().getAllPendingTutor().then((onValue) {
      for (var i = 0; i < onValue.length; i++) {
        tutors.add(User.fromJson(onValue[i]));
      }
      setState(() {
        isLoading = false;
        // _builtRow = _buildRow(tutors);
        _listOfExpansions1 = generateList(tutors);
        tutorsSelectedList = generateSeletedTutorList();
        print(_listOfExpansions1);
        // if (tutors.length == 0)
        // isEmpty = true;
        // else
        // isEmpty = false;
      });
      // _items = tutors;
    });
  }

  List<Row> _buildRow(List _tutor) {
    List<Row> row = [];
    print(_tutor.length);
    for (int i = 0; i < _tutor.length; i++) {
      row.add(_buildSingleRow(_tutor[i]));
    }
    return row;
  }

  Widget _buildSingleRow(User _tutor) {
    return Row(
      children: <Widget>[
        Flexible(
            child: Checkbox(
                value: false,
                onChanged: (bool newValue) {
                  // onChanged(newValue);
                })),
        ListView(children: [
          ExpansionTile(
            title: Text("Expansion $_tutor.name"),
            children: _listViewData
                .map((data) => ListTile(
                      leading: Icon(Icons.person),
                      title: Text(data),
                      subtitle: Text("a subtitle here"),
                    ))
                .toList(),
          ),
        ])
      ],
    );
  }

  static void handleOnExpand(bool value) {
    print("expand");
  }

  ListView generateSeletedTutorList() {
    List<ListTile> row = [];
    row.add(
      ListTile(
        title: Text("Tutors Selected"),
      ),
    );
    for (int i = 0; i < tutorsSelected.length; i++) {
      print(tutorsSelected[i].name);
      row.add(ListTile(
        title: Text(tutorsSelected[i].name),
        trailing: IconButton(
          icon: Icon(Icons.remove_circle),
          onPressed: () {
            tutorsSelected.remove(tutorsSelected[i]);
            this.setState(() {
              tutorsSelected = tutorsSelected;
            });
            _listOfExpansions1 = generateList(tutors);
            tutorsSelectedList = generateSeletedTutorList();
          },
        ),
      ));
    }
    return ListView(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        // scrollDirection: Axis.horizontal,
        // itemExtent: 100.0,
        // itemCount: tutorsSelected.length,
        padding: EdgeInsets.all(8.0),
        children: row);
  }

  List<ExpansionTile> generateList(List _tutors) {
    return List<ExpansionTile>.generate(
        _tutors.length,
        (i) => ExpansionTile(
            title: Text(_tutors[i].name),
            children: <Widget>[
              ListTile(
                // leading: Icon(Icons.person),
                title: Text("Matric Number"),
                subtitle: Text(_tutors[i].matricNum),
              ),
              ListTile(
                // leading: Icon(Icons.person),
                title: Text("Gender"),
                subtitle: Text(_tutors[i].gender),
              ),
              ListTile(
                // leading: Icon(Icons.person),
                title: Text("Date of Birth"),
                subtitle: Text(_tutors[i].dob.split("T")[0]),
              ),
              ListTile(
                // leading: Icon(Icons.person),
                title: Text("Contact Number"),
                subtitle: Text(_tutors[i].contactNum),
              ),
              ListTile(
                // leading: Icon(Icons.person),
                title: Text("Created Date"),
                subtitle: Text(
                    _tutors[i].createdDt.replaceAll("T", " ").split(".")[0]),
              ),
            ],
            onExpansionChanged: (onvalue) {
              if (!tutorsSelected.contains(_tutors[i])) {
                tutorsSelected.add(_tutors[i]);
                print("list");
                print(_tutors[i].name);
                this.setState(() {
                  tutorsSelected = tutorsSelected;
                });
                _listOfExpansions1 = generateList(tutors);
                tutorsSelectedList = generateSeletedTutorList();
              }
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Expandable ListView Example'),
        ),
        body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              (isLoading)
                  ? Flexible(
                      child: Text("Loading"),
                      flex: 10,
                    )
                  : Expanded(
                      child: ListView(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        padding: EdgeInsets.all(8.0),
                        children: _listOfExpansions1.map((a) => a).toList(),
                      ),
                      flex: 10,
                    ),
              Divider(),
              Expanded(
                child: (tutorsSelectedList != null)
                    ? tutorsSelectedList
                    : new Container(width: 0.0, height: 0.0),
                flex: 3,
              ),
              Expanded(
                child: Row(children: <Widget>[
                  IconButton(
                    tooltip: "Verify",
                    icon: Icon(Icons.check),
                    onPressed: () {
                      print("tutor");
                      print(tutorsSelected);
                      List<String> uidList = [];
                      for (var item in tutorsSelected) {
                        print("1111111111 " + item.uid);
                        uidList.add(item.uid);
                      }
                      print("uid");
                      print(uidList);
                      new SignupRestApi()
                          .verifyTutor(uidList, context)
                          .then((val) {
                        var message = (val == 0)
                            ? "The tutor(s) is/are verified"
                            : "Please try again later";
                        return showDialog(
                          context: context,
                          builder: (context) {
                            List<User> newtutors = [];
                            ListView empty;
                            new SignupRestApi()
                                .getAllPendingTutor()
                                .then((onValue) {
                              for (var i = 0; i < onValue.length; i++) {
                                newtutors.add(User.fromJson(onValue[i]));
                              }
                              this.setState(() {
                                tutors = newtutors;
                                tutorsSelectedList = empty;
                                tutorsSelected = [];
                              });
                              _listOfExpansions1 = generateList(tutors);
                              tutorsSelectedList = generateSeletedTutorList();
                              print("set state");
                              return null;
                            });
                            return AlertDialog(
                              content: Text(message),
                            );
                          },
                        );
                      });
                    },
                  ),
                  IconButton(
                    tooltip: "Reject",
                    icon: Icon(Icons.not_interested),
                    onPressed: () {
                      List<String> uidList = [];
                      for (var item in tutorsSelected) {
                        uidList.add(item.uid);
                      }
                      new SignupRestApi().rejectTutor(uidList).then((val) {
                        var message = (val == 0)
                            ? "The tutor(s) is/are rejected"
                            : "Please try again later";
                        return showDialog(
                          context: context,
                          builder: (context) {
                            List<User> newtutors = [];
                            ListView empty;
                            new SignupRestApi()
                                .getAllPendingTutor()
                                .then((onValue) {
                              for (var i = 0; i < onValue.length; i++) {
                                newtutors.add(User.fromJson(onValue[i]));
                              }
                              this.setState(() {
                                tutors = newtutors;
                                tutorsSelectedList = empty;
                                tutorsSelected = [];
                              });
                              _listOfExpansions1 = generateList(tutors);
                              tutorsSelectedList = generateSeletedTutorList();
                              print("set state");
                              return null;
                            });
                            return AlertDialog(
                              content: Text(message),
                            );
                          },
                        );
                      });
                    },
                  ),
                ]),
                flex: 2,
              ),
            ]));
  }
}

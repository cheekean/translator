import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'expandableListView.dart';
import '../signup/signupRestApi.dart';
import 'googleSignIn.dart';
import 'tutorSignupForm.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Stack(
            children: <Widget>[
          isLoading
              ? Stack(
                  children: [
                    new Opacity(
                      opacity: 0.3,
                      child: const ModalBarrier(
                          dismissible: false, color: Colors.grey),
                    ),
                    new Center(
                      child: new CircularProgressIndicator(),
                    ),
                  ],
                )
              : null,
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "LANGUAGUE LEARNING TOOL\nCREATE AN ACCOUNT NOW",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = 2.5
                      ..color = Colors.deepPurple[300],
                    fontSize: 28,
                  ),
                ),
                SizedBox(height: 50),
                _signInButton("TUTOR"),
                SizedBox(height: 10),
                _signInButton("STUDENT"),
                IconButton(
                    icon: Icon(Icons.rate_review),
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return ExpandableListView();
                          },
                        ),
                      );

                      return ExpandableListView();
                    }),
                // _tutorSignInButton(context)
              ],
            ),
          )
        ].where((child) => child != null).toList()),
      ),
    );
  }

  Widget _signInButton(String type) {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () {
        this.setState(() {
          isLoading = true;
        });
        signInWithGoogle().then((_) {
          if (_ == 'SUCCESS') {
            GoogleSignIn googleSignIn = GoogleSignIn();
            googleSignIn.signInSilently().then((account) {
              SignupRestApi().accountExist(account.id).then((isExist) {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return (isExist)
                      ? AlertDialog(
                          content: Text(
                              'This Google Account was registered. Please use another account'),
                        )
                      : TutorSignupForm(type);
                }));
              });
            });
          } else if (_ == "NO_CONNECTION")
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return AlertDialog(
                content: Text(
                    'Please make sure you have connection to the Internet'),
              );
            }));
          else {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return AlertDialog(
                content: Text('No account is selected'),
              );
            }));
          }
          this.setState(() {
            isLoading = false;
          });
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.grey),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Image(image: AssetImage("../assets/google_logo.png"), height: 35.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'SIGN UP AS ' + type,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

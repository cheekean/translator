import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'signupPage.dart';
import '../utils/networkUtil.dart';

enum Gender { MALE, FEMALE }

class SignupRestApi {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = "http://localhost:3001";
  // "http://192.168.43.167:3001"; //"http://localhost:3001";
  static final SIGNUP_URL = BASE_URL + "/signup";
  static final _API_KEY = "somerandomkey";
  static final GET_ALL_TUTOR = SIGNUP_URL + "/getAllTutor";
  static final VERIFY_TUTOR = SIGNUP_URL + "/verification";
  static final REJECT_TUTOR = SIGNUP_URL + "/rejection";
  static final ACCOUNT_EXIST = SIGNUP_URL + "/exist";

  Future<void> tutorSignup(
      String uid,
      String matricNum,
      String contactNum,
      String role,
      Gender gender,
      String name,
      String dob,
      String email,
      BuildContext context) {
    String message = "";
    var body = json.encode({
      "uid": uid,
      "matric_num": matricNum,
      "contact_num": contactNum,
      "role": role,
      "gender": gender.toString().split(".")[1],
      "name": name,
      "dob": dob,
      "email": email
    });
    return _netUtil
        .post(SIGNUP_URL, body: body.toString())
        .then((int res) async {
      if (res == 10) {
        message = 'Please fill in all the required fields.';
        await showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text(message),
                ));
      } else if (res == 0) {
        message = (role == "TUTOR")
            ? 'Personal information saved. Please wait for the response email from administrator.'
            : "Registered Successfully";
      } else if (res == 1) {
        message =
            'The account with this Google account has been registered. Please use another Google account.';
      }
      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                content: Text(message),
              )).then((val) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SignupPage()),
        );
      });
    });
  }

  Future<dynamic> getAllPendingTutor() {
    return _netUtil.get(GET_ALL_TUTOR).then((dynamic res) {
      return res;
    });
  }

  Future<int> verifyTutor(List<String> tutorUidList, context) {
    var body = json.encode({"tutorUidList": tutorUidList});
    return _netUtil.post(VERIFY_TUTOR, body: body.toString()).then((int res) {
      return res;
    });
  }

  Future<int> rejectTutor(List<String> tutorUidList) {
    var body = json.encode({"tutorUidList": tutorUidList});
    return _netUtil.post(REJECT_TUTOR, body: body.toString()).then((int res) {
      return res;
    });
  }

  Future<bool> accountExist(String uid) {
    var body = json.encode({"uid": uid});
    return _netUtil.post(ACCOUNT_EXIST, body: body.toString()).then((int res) {
      return (res == 1) ? true : false;
    });
  }
}

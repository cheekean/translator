import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../signup/signupRestApi.dart';

GoogleSignInAccount googleSignInAccount;

class TutorSignupForm extends StatefulWidget {
  String type;
  TutorSignupForm(String type) {
    this.type = type;
  }

  @override
  _TutorSignupState createState() => _TutorSignupState();
}

class _TutorSignupState extends State<TutorSignupForm> {
  Gender gender = Gender.FEMALE;
  TextEditingController nameEditingController = TextEditingController();
  TextEditingController matricNumberEditingController = TextEditingController();
  TextEditingController contactNumberEditingController =
      TextEditingController();
  TextEditingController dobEditingController = TextEditingController();
  FirebaseUser currentUser;
  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    GoogleSignIn googleSignIn = new GoogleSignIn();
    googleSignIn.signInSilently().then((onValue) {
      googleSignInAccount = onValue;
    });
    dobEditingController.text = selectedDate.year.toString() +
        "-" +
        selectedDate.month.toString() +
        "-" +
        selectedDate.day.toString();
    super.initState();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1970, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        dobEditingController.text = selectedDate.year.toString() +
            "-" +
            selectedDate.month.toString() +
            "-" +
            selectedDate.day.toString();
      });
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LayoutBuilder(builder: (context, constraints) {
      return SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: ConstrainedBox(
            constraints: BoxConstraints(
                minWidth: constraints.maxWidth,
                minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(24),
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Flexible(
                          child: SizedBox(
                            height: 50,
                          ),
                          flex: 2),
                      Flexible(
                          child: Center(
                              child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 60,
                                child: Text(
                                  (widget.type == "TUTOR")
                                      ? "LANGUAGE LEARNING TOOL\nTUTOR SIGN UP"
                                      : "LANGUAGE LEARNING TOOL\nSTUDENT SIGN UP",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    foreground: Paint()
                                      ..style = PaintingStyle.stroke
                                      ..strokeWidth = 1.5
                                      ..color = Colors.deepPurple[300],
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 50,
                                child: Text(
                                  "Some information are required\nfor signing up",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ],
                          )),
                          flex: 3),
                      Flexible(
                          child: TextFormField(
                            autofocus: false,
                            obscureText: false,
                            controller: nameEditingController,
                            validator: (value) {
                              if (value == "") return "Please enter your name";
                              return null;
                            },
                            decoration: InputDecoration(
                                labelText: "Name",
                                hintText: "Name",
                                labelStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: Colors.green,
                                        style: BorderStyle.solid))),
                          ),
                          flex: 3),
                      Flexible(
                          child: SizedBox(
                            height: 10,
                          ),
                          flex: 1),
                      Flexible(
                          child: TextFormField(
                            autofocus: false,
                            obscureText: false,
                            controller: matricNumberEditingController,
                            validator: (value) => _validateMatricNumber(value),
                            decoration: InputDecoration(
                                labelText: "Matric Number",
                                hintText: "Matric Number",
                                labelStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: Colors.green,
                                        style: BorderStyle.solid))),
                          ),
                          flex: 3),
                      Flexible(
                          child: SizedBox(
                            height: 10,
                          ),
                          flex: 1),
                      Flexible(
                          child: TextFormField(
                            controller: dobEditingController,
                            onTap: () => _selectDate(context),
                            decoration: InputDecoration(
                                labelText: "Date of Birth",
                                hintText: "Date of Birth",
                                labelStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: Colors.green,
                                        style: BorderStyle.solid))),
                          ),
                          flex: 3),
                      Flexible(
                          child: SizedBox(
                            height: 10,
                          ),
                          flex: 1),
                      Flexible(
                          child: TextFormField(
                            autofocus: false,
                            controller: contactNumberEditingController,
                            validator: (value) => validatePassword(value),
                            decoration: InputDecoration(
                                labelText: "Contact Number",
                                hintText: "0123456789",
                                labelStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1,
                                        color: Colors.green,
                                        style: BorderStyle.solid))),
                          ),
                          flex: 3),
                      Flexible(
                          child: SizedBox(
                            height: 10,
                          ),
                          flex: 1),
                      Flexible(
                          child: Column(
                            children: <Widget>[
                              Text('Gender'),
                              ListTile(
                                  title: const Text('MALE'),
                                  leading: Radio(
                                    value: Gender.MALE,
                                    groupValue: gender,
                                    onChanged: (value) {
                                      setState(() {
                                        gender = value;
                                      });
                                    },
                                  )),
                              ListTile(
                                  title: const Text('FEMALE'),
                                  leading: Radio(
                                    value: Gender.FEMALE,
                                    groupValue: gender,
                                    onChanged: (value) {
                                      setState(() {
                                        gender = value;
                                      });
                                    },
                                  ))
                            ],
                          ),
                          flex: 3),
                      Flexible(
                          child: SizedBox(
                            height: 30,
                          ),
                          flex: 1),
                      Flexible(
                          child: ButtonTheme(
                              minWidth: double.infinity,
                              child: MaterialButton(
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    new SignupRestApi().tutorSignup(
                                        googleSignInAccount.id,
                                        matricNumberEditingController.text,
                                        contactNumberEditingController.text,
                                        widget.type,
                                        gender,
                                        nameEditingController.text,
                                        dobEditingController.text,
                                        googleSignInAccount.email,
                                        context);
                                  }
                                },
                                textColor: Colors.white,
                                color: Colors.green,
                                height: 50,
                                child: Text("Submit"),
                              )),
                          flex: 3)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }));
  }

  String _validateMatricNumber(String value) {
    if (value.isEmpty) {
      return "Enter matric number";
    }

    RegExp regExp = new RegExp(r"^[\w]*$");

    if (regExp.hasMatch(value)) {
      return null;
    }

    return 'Matric number format not valid';
  }

  String validatePassword(value) {
    if (value.isEmpty) {
      return "Enter contact number";
    } else if (value.length < 10) {
      return "Contact number must be more than 9 digits";
    }
    RegExp regExp = new RegExp(r"^[0-9]*$");

    if (regExp.hasMatch(value)) {
      return null;
    }

    return 'Provide contact number with no \'-\' and digits only';
  }
}

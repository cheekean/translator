import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'Choose_Language.dart';
import '../models/language.dart';
import 'package:translator/translator.dart';
import 'screen/TtsHelper.dart';
import 'package:share/share.dart';
import 'translationHistory.dart';
import './addToServer.dart';

class TextTranslatePage extends StatefulWidget {
  TextTranslatePage(
      {Key key, this.firstLanguage, this.secondLanguage, this.onCloseClicked})
      : super(key: key);

  final Language firstLanguage;
  final Language secondLanguage;
  final Function(bool) onCloseClicked;

  @override
  TextTranslatePageState createState() => TextTranslatePageState();
}

class TextTranslatePageState extends State<TextTranslatePage>
    with SingleTickerProviderStateMixin {
  Language _firstLanguage = Language('En', 'English', true, true, true, "en");
  Language _secondLanguage = Language('Fr', 'French', true, true, true, "fr");

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  FocusNode _textFocusNode = FocusNode();
  TextEditingController _textEditingController = TextEditingController();
  String _textTranslated = "";

  GoogleTranslator _translator = new GoogleTranslator();
  Timer searchOnStoppedTyping;
  AddToServer addToServer = new AddToServer();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    this._textFocusNode.dispose();
    super.dispose();
  }

  _onLanguageChanged(Language firstCode, Language secondCode) {
    this.setState(() {
      this._firstLanguage = firstCode;
      this._secondLanguage = secondCode;
    });
  }

  _onTextChanged(String text) {
    if ((text != "") || (text != null)) {
      _translator
          .translate(text,
              from: this._firstLanguage.code, to: this._secondLanguage.code)
          .then((translatedText) {
        this.setState(() {
          this._textTranslated = translatedText;
        });
      });
      print(this._textTranslated.toString());

      if ((this._textEditingController.text != "") &&
          (this._textEditingController.text != null &&
              (this._textTranslated.toString() != "null") &&
              (this._textTranslated.toString() != ""))) {
        const duration = Duration(
            milliseconds:
                5000); // set the duration that you want call search() after that.
        if (searchOnStoppedTyping != null) {
          setState(() => searchOnStoppedTyping.cancel()); // clear timer
        }
        setState(() => searchOnStoppedTyping =
            new Timer(duration, () => _insertData(text)));
      }
    } else {
      this.setState(() {
        this._textTranslated = "";
      });
    }
  }

  _insertData(text) {
    addToServer.addHistory(text, this._textTranslated, this._firstLanguage.name,
        this._secondLanguage.name, this._secondLanguage.voiceCode);
  }

  _voiceSpeakSecondLang() {
    debugPrint(this._secondLanguage.voiceCode);
    return (TtsHelper.instance.setLanguageAndSpeak(
        this._textTranslated.toString(), this._secondLanguage.voiceCode));
  }

  _voiceSpeakFirstLang() {
    debugPrint(this._firstLanguage.voiceCode);
    return (TtsHelper.instance.setLanguageAndSpeak(
        this._textEditingController.text.toString(),
        this._firstLanguage.voiceCode));
  }

  _resultText() {
    // this._textEditingController.text = "Hello";
    //   this._textTranslated = "你好啊";
    if (this._textEditingController.text.toString() != null &&
        this._textEditingController.text.toString() != "") {
      if (this._textTranslated.toString() == "null") {
        return "Server Error. Please Try Again.";
      } else {
        return this._textTranslated.toString();
      }
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Text Translate"),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Hisotry()),
              );
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          ChooseLanguage(
            onLanguageChanged: this._onLanguageChanged,
          ),
          Card(
            color: Colors.white,
            margin: EdgeInsets.all(1.0),
            elevation: 2.0,
            child: Container(
              height: 200.0,
              child: (
                Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 16.0),
                    child: TextField(
                      style: TextStyle(fontSize: 18.0),
                      controller: this._textEditingController,
                      onChanged: this._onTextChanged,
                      maxLines: 3,
                      decoration: InputDecoration(
                          hintText: "Enter Text",
                          border: InputBorder.none,
                          suffixIcon: Column(
                            children: <Widget>[
                              RawMaterialButton(
                                onPressed: () {
                                  this.setState(() {
                                    this._textTranslated = "";
                                    this._textEditingController.text = "";
                                  });
                                  // if (this._textEditingController.text != "") {
                                  //   this.setState(() {
                                  //     this._textEditingController.clear();
                                  //     this._textTranslated = null;
                                  //   });
                                  // } else {
                                  //   this.widget.onCloseClicked(false);
                                  // }
                                },
                                child: Icon(
                                  Icons.close,
                                  color: Colors.grey,
                                ),
                                shape: CircleBorder(),
                              ),
                              RawMaterialButton(
                                onPressed: () {
                                  this._voiceSpeakFirstLang();
                                },
                                child: Icon(
                                  Icons.volume_up,
                                  color: Colors.grey,
                                ),
                                shape: CircleBorder(),
                              ),
                            ],
                          )),
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 30.0,
                      margin: EdgeInsets.only(left: 16.0),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          this._resultText(),
                          style: TextStyle(
                            color: Colors.blue[700],
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
            ),
          ),
          Card(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                  color: Colors.white,
                  child: FlatButton(
                    padding: EdgeInsets.only(
                      left: 8.0,
                      right: 8.0,
                      top: 0.0,
                      bottom: 2.0,
                    ),
                    onPressed: () {
                      Clipboard.setData(
                          new ClipboardData(text: this._textTranslated));
                      _scaffoldKey.currentState.showSnackBar(
                          new SnackBar(content: Text('Copied to Clipboard')));
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(
                          Icons.content_copy,
                          size: 20.0,
                          color: Colors.blue[800],
                        ),
                        Text(
                          "Copy",
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                ),
                Material(
                  color: Colors.white,
                  child: FlatButton(
                    padding: EdgeInsets.only(
                      left: 8.0,
                      right: 8.0,
                      top: 0.0,
                      bottom: 2.0,
                    ),
                    onPressed: () {
                      Share.share(this._textTranslated.toString());
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(
                          Icons.share,
                          size: 20.0,
                          color: Colors.blue[800],
                        ),
                        Text(
                          "Share",
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                ),
                Material(
                  color: Colors.white,
                  child: FlatButton(
                    padding: EdgeInsets.only(
                      left: 8.0,
                      right: 8.0,
                      top: 0.0,
                      bottom: 2.0,
                    ),
                    onPressed: () {
                      this._voiceSpeakSecondLang();
                    },
                    child: Column(
                      children: <Widget>[
                        Icon(
                          Icons.volume_up,
                          size: 20.0,
                          color: Colors.blue[800],
                        ),
                        Text(
                          "Listen",
                          style: TextStyle(fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

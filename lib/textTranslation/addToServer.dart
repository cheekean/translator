import 'networkUtil.dart';
import 'dart:convert';
import 'dart:async';

class AddToServer {
//Pass Data to server
  NetworkUtil _netUtil = new NetworkUtil();
  static final baseURL = "http://localhost:3001";
  static final historyURL = baseURL + "/textTranslation";
  static final addHistoryURL = historyURL + "/addHistory";

  Future<void> addHistory(
      String enteredText,
      String translatedText,
      String langTranslateTo,
      String langTranslateFrom,
      String langTranslatedCode) {
    var body = json.encode({
      "entered_text": enteredText,
      "translated_text": translatedText,
      "lang_translateTo": langTranslateTo,
      "lang_translateFrom": langTranslateFrom,
      "lang_translated_code": langTranslatedCode,
    });

    return _netUtil
        .post(addHistoryURL, body: body.toString())
        .then((int res) async {
      if (res == 200) {
        print("ADD successfully.");
      } else {
        print("Error");
      }
    });
  }
}

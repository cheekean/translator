import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'screen/TtsHelper.dart';
import 'package:share/share.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

class Hisotry extends StatefulWidget {
  Hisotry({Key key}) : super(key: key);

  @override
  HisotryState createState() => HisotryState();
}

class HisotryState extends State<Hisotry> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  static final baseURL = "http://localhost:3001";
  static final historyURL = baseURL + "/textTranslation";
  static final getHistory = historyURL + "/getAllHistory";

  Widget _displayCard(int index) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
      ),
      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 7.5),
      child: Container(
        height: 160.0,
        padding: EdgeInsets.only(left: 16.0, top: 16.0, bottom: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    data[index]["lang_translateFrom"].toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 13.0,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    data[index]["text"],
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    "\n",
                    maxLines: 1,
                  ),
                  Text(
                    data[index]["lang_translateTo"].toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 13.0,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    data[index]["translate_text"],
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                    ),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                IconButton(
                  onPressed: () {
                    Clipboard.setData(
                        ClipboardData(text: data[index]["translate_text"]));
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(content: Text("Copied To Clipboard")));
                  },
                  icon: Icon(
                    Icons.content_copy,
                    size: 20.0,
                    color: Colors.blue[800],
                  ),
                ),
                IconButton(
                  onPressed: () {
                    Share.share(data[index]["translate_text"]);
                  },
                  icon: Icon(
                    Icons.share,
                    size: 20.0,
                    color: Colors.blue[800],
                  ),
                ),
                IconButton(
                  onPressed: () {
                    TtsHelper.instance.setLanguageAndSpeak(
                      data[index]["translate_text"],
                      data[index]["lang_translateTo"],
                    );
                  },
                  icon: Icon(
                    Icons.volume_up,
                    size: 20.0,
                    color: Colors.blue[800],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  List data;

  Future<String> getData() async {
    var response = await http.get(_localhost());

    this.setState(() {
      var extractData = json.decode(response.body);
      data = extractData["data"];
    });
    print(data);

    return "Success!";
  }

  @override
  void initState() {
    this.getData();
  }

  String _localhost() {
    if (Platform.isAndroid)
      return getHistory;
    else // for iOS simulator
      return getHistory;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('History'),
      ),
      backgroundColor: Colors.grey[300],
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return _displayCard(index);
              },
            ),
          ),
        ],
      ),
    );
  }
}

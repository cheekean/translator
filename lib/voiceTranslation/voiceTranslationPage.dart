import 'dart:async';

import 'package:permission_handler/permission_handler.dart';

import 'TTSHelper.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:translator/translator.dart';
import 'Languages.dart';
import 'package:flutter/material.dart';
import 'voiceTranslationRestApi.dart';
import "../Bluetooth/PageBluetooth.dart";

class VoiceTranslationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // title: "Voice Translation",
      // debugShowCheckedModeBanner: false,
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      // ),
      // home: Scaffold(
      //   body: VoiceHome(),
      // ),

      body: VoiceHome(),
    );
  }
}

class Drawhorizontalline extends CustomPainter {
  Paint _paint;

  Drawhorizontalline() {
    _paint = Paint()
      ..color = Colors.black26
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(Offset(-150.0, 0.0), Offset(150.0, 0.0), _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class VoiceHome extends StatefulWidget {
  @override
  _VoiceHomeState createState() => _VoiceHomeState();
}

class _VoiceHomeState extends State<VoiceHome> {
  SpeechRecognition _speechRecognition;
  GoogleTranslator translator = GoogleTranslator();

  // FlutterTts flutterTts = FlutterTts();
  bool _isAvailable = false;
  bool _isListening = false;
  String outputLang = "Select one", inputLang = "Select one";
  String resultText = "";
  String textRecog = "";
  List languageList = [];
  PermissionStatus micStatus;
  VoiceTranslationRestApi voiceTranslationRestApi = VoiceTranslationRestApi();

  @override
  void initState() {
    super.initState();
    PermissionHandler()
        .checkPermissionStatus(PermissionGroup.speech)
        .then(updateMicStatus);
    Languages.comLan.forEach((key, value) {
      languageList.add({"code": key, "name": value['name']});
      print("langeages" + languageList.toString());
    });

    var d = languageList.map((f) {
      return DropdownMenuItem<String>(
        value: f["code"],
        child: Text(f["name"]),
      );
    }).toList();
    print("f" + d.toString());
    outputLang = languageList[0]["code"];
    inputLang = languageList[2]["code"];
    initSpeechRecognizer();
  }

  void listen() {
    try {
      print("llllllllllllllllllllllllllllllllllllll");
      // _speechRecognition
      //     .activate(locale: Languages.comLan[inputLang]["stt"])
      //     .then((result) => {
      //           setState(() => _isAvailable = result),
      //           print("这里" +
      //               Languages.comLan[inputLang]["stt"] +
      //               result.toString()),
      //         });
      _speechRecognition
          .listen(locale: Languages.comLan[inputLang]["stt"])
          .then((result) => print("error" + '$result' + "errorend"));
    } on Exception {
      print("ggggggggggggggggggggggggggggg");
    }
  }

  void initSpeechRecognizer() {
    _speechRecognition = SpeechRecognition();
    String text = "";
    _speechRecognition.setAvailabilityHandler((bool result) {
      setState(() => _isAvailable = result);
      if (!result) {
        final snackBar = SnackBar(
            content: Row(children: <Widget>[
          Icon(Icons.settings_voice),
          Text(
              '   Voice translation encounter error, \r\n   please stop and start again'),
        ]));

        // Find the Scaffold in the widget tree and use
        // it to show a SnackBar.
        Scaffold.of(context).showSnackBar(snackBar);
      }
      //   print("newwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww recog");
      //   _speechRecognition = new SpeechRecognition();
      //   initSpeechRecognizer();
      //   //   _speechRecognition.cancel();
      // }
    } // => setState(() => _isAvailable = result),
        );

    _speechRecognition.setRecognitionStartedHandler(
      () => setState(() => _isListening = true),
    );

    _speechRecognition.setRecognitionResultHandler(
      (String speech) => {
        print(
            "result handler####################################################################"),
        // print(speech);
        if (speech != null)
          {
            print("speechhhhh" + speech),
            // if (speech == "")
            // speech = "nothing is detected",
            text = speech
            // translator.translate(speech.toString()).then((output) => {
            //       setState(() => resultText = output.toString()),
            //       TtsHelper.instance.setLanguageAndSpeak(output, "en")
            //     })
          }
        else
          setState(() => resultText = speech)
      },
    );

    _speechRecognition.setRecognitionCompleteHandler(
      () => {
        setState(() => _isListening = false),
        print(
            "result handler******************************************************************************"),
        if (text != "")
          {
            textRecog = text,
            translator
                .translate(text.toString(),
                    from: Languages.comLan[inputLang]["translate"],
                    to: Languages.comLan[outputLang]["translate"])
                .then((output) => {
                      setState(() => resultText = output.toString()),
                      TtsHelper.instance.setLanguageAndSpeak(
                          output, Languages.comLan[outputLang]["tts"]),
                      voiceTranslationRestApi.insertNewVoiceTranslation(
                          "",
                          textRecog,
                          output,
                          Languages.comLan[inputLang]["name"],
                          Languages.comLan[outputLang]["name"],
                          context)
                    })
          },
        listen()
      },
    );

    String _currentLocale;
    _speechRecognition.setCurrentLocaleHandler(
        (String locale) => setState(() => _currentLocale = locale));

    _speechRecognition.activate().then((result) => {
          setState(() => _isAvailable = result),
          print("这里" + result.toString()),
        });

    // _speechRecognition
    //     .listen(locale: "zh_CN_#Hans")
    //     .then((result) => print('result! : $result'));
  }

  String dropdownValue = "One";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Voice Translation'),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.bluetooth),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageBluetooth()),
              );
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      // width: MediaQuery.of(context).size.width * 0.4,
                      child: DropdownButton<String>(
                          value: inputLang,
                          icon: Icon(Icons.keyboard_arrow_down),
                          iconSize: 24,
                          elevation: 16,
                          // style: TextStyle(color: Colors.deepPurple),
                          underline: Container(
                            height: 2,
                            color: Colors.grey,
                          ),
                          onChanged: (String newValue) {
                            print(newValue);
                            setState(() {
                              inputLang = newValue;
                            });
                          },
                          items: languageList.map((f) {
                            return DropdownMenuItem<String>(
                              value: f["code"],
                              child: Text(f["name"]),
                            );
                          }).toList()),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width * 0.4,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: DropdownButton<String>(
                            value: outputLang,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            elevation: 16,
                            // style: TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 2,
                              color: Colors.grey,
                            ),
                            onChanged: (String newValue) {
                              setState(() {
                                outputLang = newValue;
                              });
                            },
                            items: languageList.map((f) {
                              return DropdownMenuItem<String>(
                                value: f["code"],
                                child: Text(f["name"]),
                              );
                            }).toList()),
                      ),
                    ),
                  ]),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              padding: EdgeInsets.symmetric(
                vertical: 3.0,
                horizontal: 5.0,
              ),
              // margin: EdgeInsets.all(5.0),
              child: Text(
                "Text Recognised",
                style: TextStyle(fontSize: 24.0),
              ),
            ),
            Flexible(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                decoration: BoxDecoration(
                  color: Colors.blue[50],
                  border: Border.all(color: Colors.black38),
                  borderRadius: BorderRadius.circular(6.0),
                ),
                padding: EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 12.0,
                ),
                margin: EdgeInsets.all(5.0),
                child: Text(
                  textRecog,
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              flex: 2,
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: CustomPaint(
                painter: Drawhorizontalline(),
                // child: Center(
                //   child: Text(
                //     'Once upon a time...',
                //     style: const TextStyle(
                //       fontSize: 40.0,
                //       fontWeight: FontWeight.w900,
                //       color: Color(0xFFFFFFFF),
                //     ),
                //   ),
                // ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              padding: EdgeInsets.symmetric(
                vertical: 3.0,
                horizontal: 5.0,
              ),
              // margin: EdgeInsets.all(5.0),
              child: Text(
                "Text Translated",
                style: TextStyle(fontSize: 24.0),
              ),
            ),
            Flexible(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.blue[50],
                    border: Border.all(color: Colors.black38),
                    borderRadius: BorderRadius.circular(6.0),
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 12.0,
                  ),
                  margin: EdgeInsets.all(5.0),
                  child: Text(
                    resultText,
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                flex: 2),
            Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // FloatingActionButton(
                    //   child: Icon(Icons.cancel),
                    //   mini: true,
                    //   backgroundColor: Colors.deepOrange,
                    //   onPressed: () {
                    //     // if (_isListening)
                    //     //   _speechRecognition.cancel().then(
                    //     //         (result) => setState(() {
                    //     //           _isListening = result;
                    //     //           resultText = "";
                    //     //         }),
                    //     //       );
                    //     listen();
                    //     print(_speechRecognition.activate());
                    //   },
                    // ),
                    Container(
                      margin: EdgeInsets.all(2),
                      child: FloatingActionButton(
                        heroTag: "start",
                        child: Icon(Icons.mic),
                        onPressed: () {
                          askMicPermission();
                          if (micStatus == PermissionStatus.granted) {
                            print(_isAvailable.toString() +
                                _isListening.toString());
                            if (_isAvailable && !_isListening) {
                              print("record" +
                                  Languages.comLan[inputLang]["stt"]);
                              final snackBar = SnackBar(
                                  content: Row(children: <Widget>[
                                Icon(Icons.settings_voice),
                                Text(
                                    '   Voice translation starts, start to speak now!'),
                              ]));

                              // Find the Scaffold in the widget tree and use
                              // it to show a SnackBar.
                              Scaffold.of(context).showSnackBar(snackBar);
                              listen();
                            }
                          }
                        },
                        backgroundColor: Colors.green,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(2),
                      child: FloatingActionButton(
                        heroTag: "stop",
                        child: Icon(Icons.stop),
                        // mini: true,
                        backgroundColor: Colors.red,
                        onPressed: () {
                          // if (_isListening) {
                          // _speechRecognition.stop().then(
                          //       (result) =>
                          setState(() => _isListening = false);
                          final snackBar = SnackBar(
                              content: Row(children: <Widget>[
                            Icon(Icons.stop),
                            Text('   Voice translation stops'),
                          ]));

                          // Find the Scaffold in the widget tree and use
                          // it to show a SnackBar.
                          Scaffold.of(context).showSnackBar(snackBar);
                          //     );
                          // if (_isListening)
                          // _speechRecognition.cancel().then(
                          //       (result) => setState(() {
                          //         _isListening = result;
                          //         resultText = "";
                          //       }),
                          //     );
                          // listen();
                          // print(_speechRecognition.activate());
                          initSpeechRecognizer();
                          // }
                        },
                      ),
                    ),
                  ],
                ),
                flex: 1),
          ],
        ),
      ),
    );
  }

  void askMicPermission() {
    PermissionHandler()
        .requestPermissions([PermissionGroup.speech]).then(onStatusRequested);
  }

  void onStatusRequested(Map<PermissionGroup, PermissionStatus> value) {
    final status = value[PermissionGroup.speech];
    updateMicStatus(status);
  }

  void updateMicStatus(PermissionStatus status) {
    if (micStatus != status) {
      print("stattus" + status.toString());
      setState(() {
        micStatus = status;
      });
    }
  }
}
